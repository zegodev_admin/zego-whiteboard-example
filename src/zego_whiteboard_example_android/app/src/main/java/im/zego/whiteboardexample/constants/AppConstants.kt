package im.zego.whiteboardexample.constants


object AppConstants {

    // 思源字体路径
    const val FONT_FAMILY_DEFAULT_PATH = "fonts/SourceHanSansSC-Regular.otf"
    const val FONT_FAMILY_DEFAULT_PATH_BOLD = "fonts/SourceHanSansSC-Bold.otf"

    // 日志文件夹名称
    const val LOG_SUBFOLDER = "zegologs"

    // 日志文件大小
    const val LOG_SIZE = 5 * 1024 * 1024.toLong()

    // 图片下载文件夹名称
    const val IMAGE_SUBFOLDER = "images"

}