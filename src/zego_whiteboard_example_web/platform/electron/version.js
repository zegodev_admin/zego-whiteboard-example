var zegoSDKConfig = {
    path: './platform/electron/init_express.js',
    liveroom: '../../sdk/v1210/zego-liveroom-whiteboard-electron/ZegoLiveRoom.js',
    liveroomWb: '../../sdk/v1210/zego-liveroom-whiteboard-electron/ZegoWhiteBoardView.js',
    express: '../../sdk/v1210/zego-express-engine-electron/ZegoExpressEngine.js',
    expressWb: '../../sdk/v1210/zego-express-engine-electron/ZegoWhiteBoardView.js',
    docs: '../../sdk/v1210/zego-express-docsview-electron'
};
