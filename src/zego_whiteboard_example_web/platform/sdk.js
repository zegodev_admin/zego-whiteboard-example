function loadScript(url) {
    return new Promise((resolve) => {
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');

        script.type = 'text/javascript';
        script.src = url;
        if (script.readyState) {
            //IE
            script.onreadystatechange = function() {
                if (script.readyState == 'loaded' || script.readyState == 'complete') {
                    script.onreadystatechange = null;
                }
            };
        } else {
            //Others
            script.onload = function() {
                console.log(url + '加载成功');
                resolve();
            };
        }
        head.appendChild(script);
    });
}

// 根据平台引入对应的文件
loadScript(typeof module === 'object' ? './platform/electron/init.js' : './platform/web/init.js');
