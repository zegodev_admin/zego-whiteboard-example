/*
 * @Description: 开发环境相关配置
{
    roomid: "",
    username: "",

    whiteboard_env: "-test,-alpha",
    docs_env: "test,alpha",
    dynamicPPT_HD: "false",
    pptStepMode: "1",
    thumbnailMode: "1,2,3",
    fontFamily: "ZgFont"
}
*/

// 环境设置
$('#env-btn').click(function () {
    $('#reg-log').prop('checked', false);
});
// 登录
$('#login').click(function () {
    var username = $('#username').val();
    var roomid = $('#roomid').val();

    if (!username || !roomid) {
        alert('请输入用户名和roomID');
        return;
    }

    var conf = {
        whiteboard_ver: $('#whiteboard_ver').val(),
        docs_ver: $('#docs_ver').val(),
        whiteboard_env: $('#whiteboard_env').val(),
        docs_env: $('#docs_env').val(),
        fontFamily: $('#fontFamily').val(),
        dynamicPPT_HD: $('#dynamicPPT_HD').val(),
        pptStepMode: $('#pptStepMode').val(),
        thumbnailMode: $('#thumbnailMode').val(),
        roomid: roomid,
        username: username
    };
    sessionStorage.setItem('zegoConfig', JSON.stringify(conf));
    initZegoConfig();
});

/**
 * 开源代码时需要改动这里的配置，引入SDK的相对路径（相对index.html的路径）
 */
var zegoSDKPathList = ['./platform/web/ZegoExpressWhiteboardWeb.js', './platform/web/ZegoExpressDocsWeb.js'];
initZegoConfig();

function initZegoConfig() {
    zegoConfig = JSON.parse(sessionStorage.getItem('zegoConfig'));
    if (zegoConfig) {
        /**
         * 开源代码时只需要改动这里的配置，注意屏蔽账号相关信息（appID、tokenUrl）
         */
        // 替换成在 ZEGO 注册的 appID
        var appID = 0;
        // ** tokenUrl 仅在测试环境生效，正式环境请调用自己的后台接口，token 生成方法请参考 ZEGO 开发者中心 **
        var tokenUrl = 'https://wsliveroom-alpha.zego.im:8282/token';

        Object.assign(zegoConfig, {
            appID: appID,
            server: `wss://webliveroom${zegoConfig.whiteboard_env || appID + '-api'}.zego.im/ws`,
            userid: createUserID(),
            isDocTestEnv: !!zegoConfig.docs_env,
            tokenUrl: tokenUrl,
            isTouch: 'ontouchstart' in document.body
        });
        loadScript('./platform/web/demo.js').then(function () {
            loadAllScript(zegoSDKPathList);
        });
        $('.login_container').css('display', 'none');
        $('.whiteboard_container').css('display', 'block');
    } else {
        loadScript('./platform/web/demo.js');
        $('.whiteboard_container').css('display', 'none');
        $('.login_container').css('display', 'block');
    }
}

function createUserID() {
    var userID = sessionStorage.getItem('zegouid') || 'web' + new Date().getTime();
    sessionStorage.setItem('zegouid', userID);
    return userID;
}

function loadAllScript(sdkPathList) {
    var tasks = sdkPathList.map(function (path) {
        return loadScript(path);
    });
    if (zegoConfig.isTouch) {
        tasks.unshift(loadScript('./lib/vconsole.js').then(() => new VConsole()));
    }
    Promise.all(tasks).then(() => loadScript('./biz.js'));
}

// SDK 初始化
var zegoWhiteboard;
var zegoDocs;
var userIDList = [];

function loginRoom() {
    return new Promise((resolve) => {
        $.get(
            zegoConfig.tokenUrl, {
                app_id: zegoConfig.appID,
                id_name: zegoConfig.userid
            },
            function (token) {
                if (token) {
                    initSDK(token);
                    onRoomUserUpdate();
                    resolve();
                }
            },
            'text'
        );

        function initSDK(token) {
            var userID = zegoConfig.userid;
            // 互动白板
            zegoWhiteboard = new ZegoExpressEngine(zegoConfig.appID, zegoConfig.server);
            zegoWhiteboard.setLogConfig({
                logLevel: 'info'
            });
            zegoWhiteboard.setDebugVerbose(false);
            // 文件转码
            zegoDocs = new ZegoExpressDocs({
                appID: zegoConfig.appID,
                userID,
                token,
                isTestEnv: zegoConfig.isDocTestEnv
            });

            zegoWhiteboard
                .loginRoom(
                    zegoConfig.roomid,
                    token, {
                        userID,
                        userName: zegoConfig.username
                    }, {
                        maxMemberCount: 10,
                        userUpdate: true
                    }
                )
                .then(() => {
                    userIDList.unshift(userID);
                    $('#roomidtext').text(`房间：${zegoConfig.roomid}`);
                    $('#idNames').html('房间所有用户ID：' + userIDList.toString());
                });
        }

        function onRoomUserUpdate() {
            zegoWhiteboard.on('roomUserUpdate', (roomID, type, list) => {
                if (type == 'ADD') {
                    list.forEach((v) => userIDList.push(v.userID));
                } else if (type == 'DELETE') {
                    list.forEach((v) => {
                        var id = v.userID;
                        var index = userIDList.findIndex((item) => id == item);
                        if (index != -1) {
                            userIDList.splice(index, 1);
                        }
                    });
                }
                $('#idNames').html('房间所有用户ID：' + userIDList.toString());
            });
        }
    });
}

function logoutRoom() {
    zegoWhiteboard.logoutRoom(zegoConfig.roomid);
    sessionStorage.removeItem('zegoConfig');
}